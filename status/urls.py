from django.conf.urls import url
from .views import index, status

urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^status', status, name='status'),
]
