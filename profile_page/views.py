from django.shortcuts import render
from .models import Profile, Expertise
# Create your views here.
response={}
def index(request):
    new_activity = Profile.objects.create(name='Savira',birthday = '29 oct',gender ='female',description='i love ice cream', email='maharanisavira29@gmail.com')
    new_activity.save()

    e1 = Expertise.objects.create(expertise='Fun Coding')
    e1.save()
    e2 = Expertise.objects.create(expertise='Modeling')
    e2.save()
    e3= Expertise.objects.create(expertise='Taking Test')
    e3.save()

    new_activity.expertise.add(e1)
    new_activity.expertise.add(e2)
    new_activity.expertise.add(e3)

    response['name'] =new_activity.name
    response['birthday'] =new_activity.birthday
    response['gender']=new_activity.gender
    response['description']=new_activity.description
    response['email']=new_activity.email
    response['expertise']=new_activity.expertise.all()
    html = 'profile_page/profile_page.html'
    return render(request, html, response)
