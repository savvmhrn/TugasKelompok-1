from django.db import models

# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length=27)
    birthday = models.CharField(max_length =6)
    gender=models.CharField(max_length=6)
    description=models.CharField(max_length=21)
    expertise=models.ManyToManyField('Expertise')
    email = models.EmailField()

class Expertise(models.Model):
    expertise = models.CharField(max_length=27)

    def __str__(self):
        return str(self.expertise)
